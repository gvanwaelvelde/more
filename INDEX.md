# more

Displays the contents of a text file or program output one page at a time

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## MORE.LSM

<table>
<tr><td>title</td><td>more</td></tr>
<tr><td>version</td><td>4.3a</td></tr>
<tr><td>entered&nbsp;date</td><td>2007-10-01</td></tr>
<tr><td>description</td><td>Display a text file or program output one page at a time</td></tr>
<tr><td>summary</td><td>Displays the contents of a text file or program output one page at a time</td></tr>
<tr><td>keywords</td><td>freedos, more</td></tr>
<tr><td>author</td><td>Jim Hall &lt;jhall@freedos.org&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Imre &lt;imre.leber@telenet.be&gt;</td></tr>
<tr><td>primary&nbsp;site</td><td>https://github.com/FDOS/more</td></tr>
<tr><td>platforms</td><td>DOS  *requires KITTEN</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, Version 2](LICENSE)</td></tr>
<tr><td>wiki&nbsp;site</td><td>http://wiki.freedos.org/wiki/index.php/More</td></tr>
</table>
